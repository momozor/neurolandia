package Blog::Controller::Home;
use Mojo::Base 'Mojolicious::Controller';

# TODO List all last/new 10 posts from database from newest to oldest posts.
sub welcome {
	my $self = shift;
 	my @results = $self->db->resultset('Post')->all;
  
	$self->render(results => [@results]);
}

1;
