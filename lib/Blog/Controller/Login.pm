package Blog::Controller::Login;
use Mojo::Base 'Mojolicious::Controller';

# ensure the password mactches the user in database
sub user_exists {
	my ($self, $username, $password) = @_;
	
	my @rows = $self->db->resultset('Admin')->all;
	
	foreach my $row (@rows) {
		if ($row->username eq $username && $row->password eq $password) {
			return 1;
		}
	}
}

# executes when user accessing /login
sub login {
	my $self = shift;
}

# POST action after submitting /login form.
sub on_login {
	my $self = shift;	
	my $username = $self->param('username');
	my $password = $self->param('password');
	
	if ($self->user_exists($username, $password)) {
		$self->stash(text_username => $username);
		$self->session(logged_in => 1);
		$self->session(username => $username)
	}
	
	else {
		$self->render(text => "Wrong username or password!", status => 403);
	}
}

# for authorized router to verify if the user is logged in
sub is_logged_in {
	my $self = shift;
	
	return 1 if $self->session('logged_in');
	
	$self->render(
		inline => "<h2>Forbidden</h2>You're not logged in. Go to <a href='/login'>login page</a>",
		status => 403,
	);
}

# admin logout
sub logout {
	my $self = shift;
	
	$self->session(expires => 1);
	$self->redirect_to('/');
}

1;
