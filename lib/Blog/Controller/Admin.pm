package Blog::Controller::Admin;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::Date;

# admin homepage
# this is the route where the logged in user can view their
# last logged in time(TODO), username, and random message.
sub overview {
    my $self = shift;
    
    $self->stash(username => $self->session('username'));
}

# where admin write their posts
# using CKEditor (the only quick and working choice I found)
sub write {
    my $self = shift;

    $self->stash(username => $self->session('username'));
}
    
# TODO include custom link so the post can be viewed in separate pages.
# FIXME check $title and $text value for their presence. I could not
# do it for some reason.
# Also, not sure if authenticating the value presence on client-side
# is actually 'safe'.
sub on_post {
    my $self = shift;
    
    my $title = $self->param('title');
    my $text = $self->param('editor');
    my $date = Mojo::Date->new;
    my $username = $self->session('username');

    my $new_post = $self->db->resultset('Post')->create({
        author => $username,
        title => $title,
        text => $text,
        published_date => $date,
    });
    
    $self->redirect_to('/');
}

1;