use utf8;
package Blog::Schema::Result::Post;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Blog::Schema::Result::Post

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Post>

=cut

__PACKAGE__->table("Post");

=head1 ACCESSORS

=head2 author

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 title

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 text

  data_type: 'varchar'
  is_nullable: 0
  size: 1200

=head2 published_date

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=cut

__PACKAGE__->add_columns(
  "author",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "title",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "text",
  { data_type => "varchar", is_nullable => 0, size => 1200 },
  "published_date",
  { data_type => "varchar", is_nullable => 0, size => 50 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-05-08 20:05:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:k21XYLSDEw6Eu4FpboS3Xw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
