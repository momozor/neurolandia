use utf8;
package Blog::Schema::Result::Admin;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Blog::Schema::Result::Admin

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<Admin>

=cut

__PACKAGE__->table("Admin");

=head1 ACCESSORS

=head2 username

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=cut

__PACKAGE__->add_columns(
  "username",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 255 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-05-08 20:05:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EY/Qs0C/osKc45TPKe27pg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
