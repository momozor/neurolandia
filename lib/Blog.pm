package Blog;
use Mojo::Base 'Mojolicious';
use Blog::Schema;

has schema => sub {
    return Blog::Schema->connect("dbi:mysql:dbname=$ENV{DB_NAME};host=$ENV{DB_HOST}", $ENV{DB_USER}, $ENV{DB_PASS});
};

# This method will run once at server start
sub startup {
  my $self = shift;

  
  # to make the accessor scope global through the app, we make
  # a little helper so schema will be automatically
  # imported and just use them from a routine in Blog::* with:
  #
  # sub do_something {
  # 	my $self = shift;
  #
  # 	$self->db->resultset('ResultName')->whatever_method;
  # }
  $self->helper(db => sub { $self->app->schema });

  # create db
  
  # Load configuration from hash returned by "my_app.conf"
  my $config = $self->plugin('Config');
  $self->app->sessions->cookie_name('Neurolandia');
  $self->plugin('TagHelpers');

  # Router
  my $r = $self->routes;

  # fundamental routes
  $r->get('/')->to('home#welcome');

  # FIXME route number still returns error
  $r->get('/post/:id')->to(cb => sub {
		my $self = shift;

		my $post_id = $self->param('id');

		$self->render(text => $post_id) if $post_id =~ /\d/;

		$self->render(text => 'error');
		
	  });

  $r->get('/login')->to('login#login');
  
  # static pages
  $r->get('/about')->to('home#about');
  $r->get('/contact')->to('home#contact');
  
  # admin logout
  $r->get('/logout')->to('login#logout');
  
  # we use any because it will return a 'redirect page' before
  # actually redirects to another route with its custom html code in
  # login.html.ep template file.
  
  $r->any('on_login')->to('login#on_login');
  
  # admin/write area
  my $authorized = $r->under('/admin')->to('login#is_logged_in');
  $authorized->get('/')->to('admin#overview');
  $authorized->get('/overview')->to('admin#overview');
  $authorized->get('/write')->to('admin#write');
  $authorized->post('/on_post')->to('admin#on_post');
}

1;
