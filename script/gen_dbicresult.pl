#!/usr/bin/env perl

use DBIx::Class::Schema::Loader qw/ make_schema_at /;
           make_schema_at(
               'Blog::Schema',
               { debug => 1,
                 dump_directory => './lib',
               },
               [ "dbi:mysql:dbname=$ENV{DB}", $ENV{DB_USER}, $ENV{DB_PASS}
               ],
);
