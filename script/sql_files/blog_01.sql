CREATE TABLE Admin(
	username VARCHAR(255) PRIMARY KEY NOT NULL,
	password VARCHAR(255) NOT NULL
);

CREATE TABLE Post(
    author VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    text VARCHAR(1200) NOT NULL,
    published_date VARCHAR(50) NOT NULL
);

INSERT INTO Admin VALUES(
	"foxy", 
	"dummy_pass"
);

INSERT INTO Admin VALUES(
	"faraco",
	"whatever5"
);

