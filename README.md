Neurolandia - Blog
---

A blog app in Perl 5 + Mojolicious web framework.

> For local test run and minimal database system, see this [branch](https://github.com/faraco/blogapp/tree/sqlite) instead.

This project is under maintainance.


Demo
---

## Homepage

![](https://cloud.githubusercontent.com/assets/24475030/26536951/ec124918-446d-11e7-9378-216a421580a6.png)

## Writing
![](https://cloud.githubusercontent.com/assets/24475030/26536897/9c69686a-446d-11e7-83f6-2e1b6d8ded02.png)

# TODO
- Comments system
- Subscribe email/rss(atom) feed 
- UD from CRUD functionality

How to deploy on Heroku with MySQL from Linux?
---
* You need git & heroku command line client to be installed in your local system before doing anything else.

* After you installed the tools above, clone the master repository by running: `git clone https://github.com/faraco/blogapp`

* After that, run `heroku apps:create my_app_name --buildpack https://github.com/judofyr/perloku.git`

* Then, add addon to your heroku stack by running: `heroku addons:create cleardb:ignite`

* Now, find your MySQL *CLEARDB_DATABASE_URL* value by running: `heroku config | grep ^CLEARDB_DATABASE_URL`
	- You'll get the value similar like this: `mysql://adffdadf2341:adf4234@us-cdbr-east.cleardb.com/heroku_db?reconnect=true`

* Now split the value into separate env variable by doing: `heroku config:set DB_USER=adffdadf2341 DB_PASS=adf4234 DB_HOST=us-cdbr-east.cleardb.com DB_NAME=heroku_db`

* Now let's create the table for the available database using our mysql command line too - `mysql -u adffdadf2341 -p -h us-cdbr-east.cleardb.com heroku_db` and use the *DB_PASS* value as the mysql password.

* Now you can safely push and build the deployment branch to your heroku application stack with: `git push heroku master`

* Open your browser, and navigate to `my_app_name.herokuapp.com` and have fun!

## License
This software is licensed under the AGPL-3.0 license. See LICENSE file for details.
