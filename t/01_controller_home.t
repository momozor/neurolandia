use lib 'lib';
use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

BEGIN {
	use_ok('Blog::Schema');
}

my $t = Test::Mojo->new('Blog');

# TODO Change the app name to something more specific and unique 
$t->get_ok('/')
	->status_is(200)
	->text_is(title => 'Home | Neurolandia');

$t->get_ok('/about')
	->status_is(200)
	->text_is(title => 'About | Neurolandia');

$t->get_ok('/contact')
	->status_is(200)
	->text_is(title => 'Contact | Neurolandia');

done_testing();
