use lib 'lib';
use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

BEGIN {
	use_ok('Blog::Schema', 'use ok Blog::Schema');
}

my $t = Test::Mojo->new('Blog');

$t->get_ok('/login')
	->status_is(200)
	->text_is(title => 'Login | Neurolandia');

# custom redirection test method
my $location_is = sub {
           my ($t, $value, $desc) = @_;
           $desc ||= "Location: $value";
           local $Test::Builder::Level = $Test::Builder::Level + 1;
           return $t->success(is($t->tx->res->headers->location, $value, $desc));
};

$t->get_ok('/logout')
	->status_is(302)
	->$location_is('/');
	
done_testing();
